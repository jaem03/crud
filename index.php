<?php
     
    include'config.php';
    include 'action.php';
    $page_title = 'Home';
    include 'template/header.php';


    ?>

        <div class ="row">
            <div class="col-md-2"> 
                <button class="btn btn-lg btn-primary" type="button" data-target="#addModal" data-toggle="modal">Add Record</button>
            </div>
        </div>

        <table class="table table-striped"> 
                <thead>
                    <tr>
                            <th>    ID </th>
                            <th>    Name </th>
                            <th>    Age </th>
                            <th>    Address </th>
                            <th>    Action </th>            
                    </tr>
                </thead>
                        <tbody id="Records"> </tbody>
        </table>

        <div id="addModal" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Record </h4>
                    <button type="button" class="close" data-dismiss="modal">x</button>

                </div>
        <form id="addForm">
                <div class="modal-body">
                    <div class ="row">
                        <div class="col-md-4">
                             <label>Name: </label>
                                <input type="text" name="name" class="form-control" placeholder="e.g. del Mundo"/> 
                        </div>
                    </div>
                <div class ="row">
                    <div class="col-md-4">
                            <label>Age: </label>
                                <input type="number" name="age" class="form-control" placeholder="e.g. 20"/>
                    </div>
                </div>
                <div class ="row">
                    <div class="col-md-4">
                            <label>Address: </label>
                                <textarea name="address" class="form-control" placeholder="e.g. Po, Oriental Mindoro"/> </textarea> 
                    </div>
                </div>
                </div>          
                        
                <div class="modal-footer">
                        <input type="hidden" name="action" value="AddRecord"/> 
                        <button class="btn btn-primary" type="submit"> Add</button>
                        <button class="btn btn-danger" type="submit" data-dismiss="modal">Close</button>
                </div>
        </form>
            </div>
            </div>
        </div>



 <div id="updateModal" class="modal" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Record </h4>
                    <button type="button" class="close" data-dismiss="modal">x</button>

                </div>
        <form id="editForm">
                <div class="modal-body">
                    <div class ="row">
                        <div class="col-md-4">
                            <label>name:</label>
                    <input type="text" name ="updateName" id="updateName" class="form-control" />
                </div>
                </div>

                <div class = "row">
                <div class="col-md-4">
                    <label> age:</label>
                    <input type="number"name ="updateAge" id="updateAge" class="form-control" />
                </div>
                </div>



                <div class = "row">
                <div class="col-md-4">
                    <label>address:</label>
                    <textarea  class="form-control" name ="updateAddress"  id="updateAddress">
                    
                </textarea>
                                </div>
                </div>
          
               
            </div>
            <div class="modal-footer">
                        <input type="hidden" name="action" value="edit"/>
                        <input type="hidden" name="id" id="u_id"/>
                        <button class="btn btn-primary" type="submit">Update</button>
                        <button class="btn btn-danger" type="submit" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


 <div id="deleteModal" class="modal" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete </h4>
                    <button type="button" class="close" data-dismiss="modal">x</button>

                </div>
        <form id="deleteForm">
                <div class="modal-body">
                   Are you sure you want to delete this record?
          
               
            </div>
            <div class="modal-footer">
                        <input type="hidden" name="action" value="remove"/>
                        <input type="hidden" name="id" id="d_id"/>
                        <button class="btn btn-primary" type="submit">Yes</button>  
                        <button class="btn btn-danger" type="submit" data-dismiss="modal">No</button>
                </div>
            </form>
        </div>
    </div>
</div>



       <?php

    include 'template/footer.php';
    ?>


    