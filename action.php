<?php

include 'config.php';

if (isset($_POST['action']) && !empty($_POST['action'])){


		$action = $_POST['action'];

	switch($action){
		case 'showRecords':
		showRecords();
		break;
		case 'AddRecord':
		AddRecord();
		break;
		case 'showUpdate':
		showUpdate();
		break;
		case 'showDelete':
		showDelete();
		break;
		case 'edit':
		Edit();
		break;
		
		
		default:
	}
}

	function showRecords() {
		include 'config.php';
		try{

			$stmt=$db_con->prepare("SELECT * FROM students");
			$stmt->execute();
			while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
				?>
				<tr>
				<td><?php echo $row['id']?></td>
				<td><?php echo $row['name']?></td>	
				<td><?php echo $row['age']?></td>
				<td><?php echo $row['address']?></td>
				<td>
				
					<a href="#" class="btn btn-primary" onclick="showUpdate (<?php echo $row['id'] ?>)">Edit</a>	
					<a href="#" class="btn btn-danger"  onclick="showDelete (<?php echo $row['id'] ?>)">Delete</a>		
					

				</td>
				</tr>
				<?php
			}
		return true;
		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}

		function AddRecord() {
		include 'config.php';
		try{ 
			$name = $_POST['name'];
			$age = $_POST['age'];
			$address = $_POST['address'];

			$stmt = $db_con->prepare("INSERT INTO students (name,age,address) VALUES (:name, :age,:address)");
			$stmt->bindparam(":name", $name);
			$stmt->bindparam(':age', $age);
			$stmt->bindparam(":address", $address);
			$stmt->execute();

			return true;

		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}


function showUpdate(){
	include 'config.php';
	$id=$_POST['id'];
	$stmt=$db_con->prepare("SELECT * FROM students WHERE id = :id");
	$stmt->bindparam(':id',$id);
	$stmt->execute();
	 $row=$stmt->fetch(PDO::FETCH_ASSOC);
	$data=array('name' => $row['name'],
	'age' => $row['age'],
	'address' => $row['address'],
	'id' => $row['id']
	);
	echo json_encode($data);
}


function Edit(){
	
		include 'config.php';
		try{ 
			$id = $_POST['id'];
			$name = $_POST['updateName'];
			$age = $_POST['updateAge'];
			$address = $_POST['updateAddress'];

		$stmt = $db_con->prepare("UPDATE students SET name = :name, age = :age, address = :address WHERE id = :id");
		$stmt->bindparam(':id', $id);
		$stmt->bindparam(":name", $name);
		$stmt->bindparam(':age', $age);
		$stmt->bindparam(":address", $address);
		$stmt->execute();

			return true;

		} catch (PDOException $ex){
			echo $ex->getMessage();
			return false;
		}
	}


function showDelete(){
include 'config.php';
$id=$_POST['id'];
$stmt=$db_con->prepare("SELECT * FROM students WHERE id = :id");
$stmt->bindparam(':id',$id);
$stmt->execute();
 $row=$stmt->fetch(PDO::FETCH_ASSOC);
$data=array('name' => $row['name'],
'age' => $row['age'],
'address' => $row['address'],
'id' => $row['id']
);
echo json_encode($data);
}




	
	
?>